def evaluate(c):
 input_list,n,t
 answer_ = 0.0
 for i in range(n):
  if ((c + input_list[i][1]) <= 0):
      return -1
  else:
      answer_+= 1.0*(input_list[i][0] / (c + input_list[i][1]))
 if(answer_ <= t):
     return 1
 return -1
 
n,t = map(int,input().split())
input_list = []
for i in range(n):
 input_list.append(list(map(int,input().split())))
left_end = -10**9
right_end = 10**9
while((right_end-left_end) > 10**-9):
 mid_value = (left_end+right_end)/2
 if(evaluate(mid_value) > 0):
     right_end = mid_value
 else:
     left_end = mid_value
print(round(left_end,9))

